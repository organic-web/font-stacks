# Local-only Font Stacks

Reduce or eliminate reliance on external resources relating to typography by choosing a local only font stack.

[demo](https://organic-web.gitlab.io/font-stacks/)

Pull requests welcome!

