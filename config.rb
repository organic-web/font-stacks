activate :autoprefixer do |prefix|
  prefix.browsers = 'last 2 versions'
end

# Build-specific config (ie, for Gitlab pages)
configure :build do
  # Gitlab hosts in a subfolder by default, need to use relative assets/links
  activate :relative_assets
  set :relative_links, true
end

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false
